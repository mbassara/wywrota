const EUROPEAN_TO_USA = {
   "c":   "Cm",  "c7":   "Cm7",

   "Cis": "C#",  "Cis7": "Db7",
   "cis": "Dbm", "cis7": "Dbm7",

   "d":   "Dm",  "d7":   "Dm7",

   "Dis": "D#",  "Dis7": "Eb7",
   "dis": "Ebm", "dis7": "Ebm7",

   "e":   "Em",  "e7":   "Em7",

   "f":   "Fm",  "f7":   "Fm7",

   "Fis": "F#",  "Fis7": "Gb7",
   "fis": "Gbm", "fis7": "Gbm7",

   "g":   "Gm",  "g7":   "Gm7",

   "Gis": "G#",  "Gis7": "Ab7",
   "gis": "Abm", "gis7": "Abm7",

   "a":   "Am",  "a7":   "Am7",

   "Ais": "A#",  "Ais7": "Bb7",
   "ais": "Bbm", "ais7": "Bbm7",

   "H":   "B",   "H7":   "B7",
   "h":   "Bm",  "h7":   "Bm7"
};

var USA_TO_EUROPEAN = {};

const CHORDS_BY_SEMITONE = [
   ["C", "C7", "Cm", "Cm7"],
   ["C#", "Db7", "Dbm", "Dbm7"],
   ["D", "D7", "Dm", "Dm7"],
   ["D#", "Eb7", "Ebm", "Ebm7"],
   ["E", "E7", "Em", "Em7"],
   ["F", "F7", "Fm", "Fm7"],
   ["F#", "Gb7", "Gbm", "Gbm7"],
   ["G", "G7", "Gm", "Gm7"],
   ["G#", "Ab7", "Abm", "Abm7"],
   ["A", "A7", "Am", "Am7"],
   ["A#", "Bb7", "Bbm", "Bbm7"],
   ["B", "B7", "Bm", "Bm7"]
];

var ALL_CHORDS = [];

for (var european in EUROPEAN_TO_USA) {
   var usa = EUROPEAN_TO_USA[european];
   USA_TO_EUROPEAN[usa] = european;
   addIfNotPresent(ALL_CHORDS, european)
}

for (var i = 0; i < CHORDS_BY_SEMITONE.length; i++) {
   var chords = CHORDS_BY_SEMITONE[i];
   for (var j = 0; j < chords.length; j++) {
      addIfNotPresent(ALL_CHORDS, chords[j]);
   }
}

function addIfNotPresent(array, element) {
   if (array.indexOf(element) === -1) array.push(element);
}

const CHORDS_GROUP_REGEX = "(?:^|\\s)(" + chordsRegex() + "|\\s|,|\\))+$";

function chordsRegex() {
   var alternative = ALL_CHORDS.sort().reverse().join("|");
   return "(" + alternative + ")";
}

function getChordType(chord) {
   for (var i = 0; i < CHORDS_BY_SEMITONE.length; i++) {
      var chords = CHORDS_BY_SEMITONE[i];
      for (var j = 0; j < chords.length; j++) {
         if (chords[j] === chord) {
            return {"semitone": i, "type": j}
         }
      }
   }
   return undefined;
}

function getChordByType(semitone, type) {
   return CHORDS_BY_SEMITONE[semitone][type];
}
