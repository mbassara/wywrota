var change = 0;
var songText = "";
var usFormat = false;

function setChange(newChange) {
   change = newChange;
   $("#change-value").text(change);
}

$("#load-button").click(function () {
   var value = $("#link").val();
   $.get(value).done(handleHttpContent);
});

$("#up-button").click(function () {
   setChange((change + 1) % 12);
   transposeAll();
});

$("#down-button").click(function () {
   setChange((change - 1) % 12);
   transposeAll();
});

$("#set-mono-font").click(function () {
   if ($(this).is(":checked")) {
      $("#content").css("font-family", "monospace");
   } else {
      $("#content").css("font-family", "");
   }
});

$("#set-us-format").click(function () {
   usFormat = $(this).is(":checked");
   transposeAll();
});

function handleHttpContent(data) {
   var siteContent = data.replace(/<img[^>]*>/g, "");      // remove <img> tags

   var siteContentElement = $("<output>").append(siteContent);
   var songContentHtml = $(".songContent", siteContentElement).html();
   if (songContentHtml.indexOf("<pre") === -1) {
      songContentHtml = songContentHtml
         .replace(/(?:\r\n|\r|\n)/g, "")                   // remove newlines
         .replace(/\s+/g, " ");                            // squash multiple whitespaces to one space
   }

   songContentHtml = songContentHtml
      .replace(/(<p>|<br ?\/?>|<div>)/ig, "\n")            // replace html newlines with \n
      .replace(/(<\/p>|<\/div>)/ig, "");                   // remove </p> and </div> tags

   songText = $("<output>").append(songContentHtml).text();

   setChange(0);
   transposeAll();
}

function transposeAll() {
   var lines = songText.split("\n");
   var transposedContent = $.map(lines, transposeLine).join("<br>");

   $("#content").empty().append(transposedContent);
}

function transposeLine(line) {
   line = line.replace(new RegExp("/\\s*?" + chordsRegex() + "\\s*?/", "g"), function (chord) {
      chord = chord.replace(/\//g, "");
      return "<b>/" + transpose(chord) + "/</b>";
   });

   var lyricsPart = line.replace(new RegExp(CHORDS_GROUP_REGEX), "");
   var chordsPart = line.replace(lyricsPart, "");

   chordsPart = chordsPart.replace(new RegExp(chordsRegex(), "g"), function (chord) {
      return "<b>" + transpose(chord) + "</b>";
   });
   return lyricsPart + chordsPart;
}

function transpose(originalChord) {
   var chord = originalChord;
   if (chord in EUROPEAN_TO_USA) {
      chord = EUROPEAN_TO_USA[chord];
   }

   var chordType = getChordType(chord);
   if (!chordType) {
      return originalChord;
   }

   var semitone = chordType.semitone;
   var newSemitone = (12 + semitone + change) % 12;


   var newChord = getChordByType(newSemitone, chordType.type);
   if (newChord in USA_TO_EUROPEAN && !usFormat) {
      newChord = USA_TO_EUROPEAN[newChord];
   }
   return newChord;
}
